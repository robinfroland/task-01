// DOM ELEMENTS

const containerElem = document.getElementById('container');
// Bank
const userNameElem = document.getElementById('user-name');
const bankCardElem = document.getElementById('bank-card');
const bankBalanceElem = document.getElementById('bank-balance');
const btnGetLoan = document.getElementById('btn-get-loan');
const systemMsgElem = document.getElementById('system-msg');

// Work
const payAmountElem = document.getElementById('pay-amount');
const btnPayElem = document.getElementById('btn-pay');
const btnWorkElem = document.getElementById('btn-work');

// Laptop
const laptopSelectionElem = document.getElementById('available-laptops');
const laptopFeaturesElem = document.getElementById('laptop-features');
const laptopImgElem = document.getElementById('laptop-img');
const laptopTitleElem = document.getElementById('laptop-title');
const laptopBodyElem = document.getElementById('laptop-body')
const laptopPriceElem = document.getElementById('laptop-price');
const btnBuyNow = document.getElementById('buy-btn');
const orderMsgElem = document.createElement('h1');


// DUMMY DATA
const laptops = [
    {id: 0, name: "Macbook Pro", description: 'Den nye MacBook Air 2020 med sitt svært lettvektige aluminiumskabinett er den ideelle hverdagsmaskinen. Den er utstyrt med Retina-skjerm og en 10. generasjons Intel Core-prosessor for produktivitet, ytelse og underholdning både hjemme og på farten.', price: 12799, image: "https://www.elkjop.no/image/dv_web_D180001002461047/161636/macbook-air-2020-133-256-gb-stellargraa.jpg", specs: ["Quad-Core i5 2.0GHz", "512GB SSD"]},
    {id: 1, name: "HP EliteBook", description: 'Den bærbare PC-en HP EliteBook x360 1030 G3 13,3" 2-i-1 har alt du behøver for en produktiv arbeidsdag. HP SureView-skjermfilteret gjør at du kan jobbe hvor som helst uten at sidemannen kan se hva du jobber med.', price: 1337, image: "https://www.elkjop.no/image/dv_web_D180001002376657/146389/hp-elitebook-x360-1030-g3-133-2-i-1-baerbar-pc-soelv.jpg", specs: ["Intel® Core™ i5", "8 GB DDR3 RAM", "256 GB SSD"]},
    {id: 2, name: "Asus M409DA", description: 'Asus M409DA-EK208T er en 14-tommers bærbar PC med et slankt design og NanoEdge Full HD-skjerm. Med effektiv AMD Ryzen 7 prosessor og integrert Radeon-grafikk får du hverdagens dataoppgaver unnagjort, og PC-en tillater også avslappet gaming på farten.', price: 7495, image: "https://www.elkjop.no/image/dv_web_D180001002480934/194399/asus-m409da-ek208t-14-baerbar-pc-soelv.jpg", specs: ["AMD Ryzen 7 3700U", "8 GB DDR4 RAM"]},
    {id: 3, name: "Samsung Thinkpad", description: 'Asus Chromebook Flip C434 14" 2-i-1 bærbar PC er lett å ta med seg farten takket være sin slanke profil på 1,5 cm og vekt på kun 1,4 kg. Videre er den utstyrt med et stilfullt aluminiumschassis og en allsidig og roterbar Full HD-touchskjerm.', price: 6995, image: "https://www.elkjop.no/image/dv_web_D180001002233683/26341/asus-chromebook-flip-c434-14-2-i-1-baerbar-pc-soelvsort", specs: ["Intel® Core™ M3-8100Y-prosessor", "4 GB LDDR3 RAM, 64 GB flashlagring"]},
]

const user = {
    name: "Robin Frøland",
    payBalance: 0,
    bankBalance: 0,
    activeLoan: false
}

// EVENTS
btnGetLoan.addEventListener('click', getLoan);
btnWorkElem.addEventListener('click', doWork);
btnPayElem.addEventListener('click', payTransfer);
btnBuyNow.addEventListener('click', buyNow);
laptopSelectionElem.onchange = renderSelection;


function getLoan() {
    const loanAmount = prompt("Please enter desired amount to loan");
    systemMsgElem.innerText = "";
    if (loanAmount == null || loanAmount === "0") {
        return;
    }

    if (loanAmount > user.bankBalance * 2 || user.activeLoan ) {
        systemMsgElem.innerText = "Application rejected"
        systemMsgElem.className = "text-loan text-danger";
    } else {
        systemMsgElem.innerText = "Loan granted"
        systemMsgElem.className = "text-loan text-success";
        user.bankBalance += parseInt(loanAmount);
        
        bankBalanceElem.innerText = user.bankBalance;
        user.activeLoan = true;
    }

}

function buyNow() {
    let laptopId = laptopSelectionElem.value;
    const laptop = laptops[laptopId]
    systemMsgElem.innerText = "";

    if (laptop.price > user.bankBalance) {
        systemMsgElem.innerText = "Insufficient funds"
        systemMsgElem.className = "text-loan text-danger";
    } else {
        user.bankBalance -= laptop.price;
        bankBalanceElem.innerText = user.bankBalance;
        systemMsgElem.className = "text-loan text-success";
        systemMsgElem.innerText = "Congratulations on your purchase of " + laptop.name;
        
        
    };
    containerElem.insertBefore(systemMsgElem, containerElem.firstChild);

}

function doWork() {
    user.payBalance += 100;
    payAmountElem.innerText = user.payBalance;
}

function payTransfer() {
    const transferAmount = user.payBalance;

    if (transferAmount != 0) {
        user.payBalance = 0;
        user.bankBalance += transferAmount;
        bankBalanceElem.innerText = user.bankBalance;
        payAmountElem.innerText = user.payBalance;
    }
    
}

function renderSelection() {
    resetSelectionDOM();
    let laptopId = laptopSelectionElem.value;
    const laptop = laptops[laptopId]

    // Update laptop attributes
    laptopImgElem.src = laptop.image;
    laptopTitleElem.innerText = laptop.name;
    laptopBodyElem.innerText = laptop.description;
    laptopPriceElem.innerHTML = laptop.price;

    // Update features
    laptop.specs.forEach((spec) => {
        const specElem = document.createElement('li');
        specElem.innerText = spec;
        laptopFeaturesElem.appendChild(specElem);
    })
    
}

function updateValues() {}


function resetSelectionDOM() {
    while (laptopFeaturesElem.firstChild) {
        laptopFeaturesElem.removeChild(laptopFeaturesElem.firstChild);
    }
}

function populateLaptopSelection(laptops) {
    laptops.forEach((laptop) => {
        const optionElem = document.createElement('option');
        optionElem.className = 'text-secondary';
        optionElem.innerText = laptop.name;
        optionElem.value = laptop.id;
        optionElem.style = "color: red;";
        laptopSelectionElem.appendChild(optionElem);
    });
}
function init(laptops) {
    populateLaptopSelection(laptops);
    systemMsgElem.innerText = "Welcome back, " + user.name;
    userNameElem.innerText = user.name;
    payAmountElem.innerText = user.payBalance;
    bankBalanceElem.innerText = user.bankBalance;
    renderSelection();

}

init(laptops);